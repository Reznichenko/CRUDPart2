﻿using BusinessLogic.Paging;
using BusinessLogic.Services;
using BusinessLogic.ViewModels;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace OrderManagement.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult LoadData(DataTableAjaxPostModel model)
        {
            if (model.search == null)
            {
                model.search = new Search { value = "" };
            }
            int totalRecords;
            int recordsFiltered;
            string currentUserId = User.Identity.GetUserId();
            var products = new ProductService().GetPaginated(model, out totalRecords, out recordsFiltered);
            var cartData = new CartService().GetProductsInCart(currentUserId);
            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalRecords,
                recordsFiltered = recordsFiltered,
                data = products,
                addedData = cartData

            }, JsonRequestBehavior.AllowGet);
        }

        public void AddToCart(int? id)
        {
            if (id == null)
            {
                throw new System.Exception();
            }

            ProductViewModel product;
            using (var productService = new ProductService())
            {
                var productList = productService.LoadViewModel();
                product = productList.Find(x => x.Id == id);
            }

            string currentUserId = User.Identity.GetUserId();           

            using (var cartService = new CartService())
            {
                var cart = cartService.CreateViewModel(product, currentUserId);
                cartService.AddToCart(cart);
            }

            
        }
    }
}
