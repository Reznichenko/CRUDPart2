﻿using BusinessLogic.Paging;
using BusinessLogic.Services;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace OrderManagement.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult LoadData(DataTableAjaxPostModel model)
        {
            if (model.search == null)
            {
                model.search = new Search { value = "" };
            }
            int totalRecords;
            int recordsFiltered;
            string currentUserId = User.Identity.GetUserId();
            var cartData = new CartService().GetPaginated(model, currentUserId, out totalRecords, out recordsFiltered);
            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalRecords,
                recordsFiltered = recordsFiltered,
                data = cartData

            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCart(DataTableAjaxPostModel model)
        {
            if (model.search == null)
            {
                model.search = new Search { value = "" };
            }
            string currentUserId = User.Identity.GetUserId();
            var data = new CartService().GetProductsInCart(currentUserId);
            return Json(new
            {
                data = data

            }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult DeleteFromCart(int id)
        {
            using (var cartService = new CartService())
            {
                cartService.Delete(id);
                return RedirectToAction("Index");
            }
        }
    }
}