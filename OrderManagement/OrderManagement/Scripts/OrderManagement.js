﻿var cartTable;
$(document).ready(function () {
    crudNamespace.buildProductTable();
    cartTable = crudNamespace.loadCartTable();
    //$(document).ajaxError(function (event, request, settings) {
    //    crudNamespace.showModal("An error occurred: " + request.status + " " + request.statusText + " URL: " + settings.url);
    //});
    
});

var crudNamespace = {

    buildProductTable: function () {
        var p1 = crudNamespace.getCart();
        p1.then(function (data) { crudNamespace.loadProductTable(data) })
    },

    loadProductTable: function (cartData) {
        $('#productTable').DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            "ajax": {
                url: "/Product/LoadData",
                type: 'POST',
            },
            "columns": [
                { "data": "Id", "autoWidth": true, "visible": false },
                { "data": "Title", "autoWidth": true },
                { "data": "CategoryName", "autoWidth": true },
                { "data": "CategoryId", "autoWidth": true, "visible": false },
                { "data": "Price", "autoWidth": true },
                { "data": "Color", "autoWidth": true },
                { "data": "VendorCode", "autoWidth": true },
                { "data": "Quantity", "autoWidth": true },
                {
                    "data": null,
                    "className": "center",
                    "render": function (data, type, row) {
                        var isInCart = crudNamespace.isElementInCart(data.Id, cartData);
                        if (isInCart == true) {
                            return '<input type="button" disabled value="In Cart" class="btn btn-basic btn-xs">';
                        }
                        else {

                            return '<input type="button" value="Add to Cart" onclick="this.value=\'In Cart\'; this.disabled = true; crudNamespace.addToCart(' + row.Id + ')" class="btn btn-basic btn-xs">';
                        }
                    },
                    "targets": 0,
                    "orderable": false
                }
            ]
        });
    },

    addToCart: function (id) {
        $.ajax({
            url: 'Product/AddToCart',
            method: "GET",
            data: { id: id },
            success: crudNamespace.onAddToCart,
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    isElementInCart: function (row, cartData) {
        var result = false;
        $.each(cartData, function (i, item) {
            $.each(item, function (index, value) {
                if (value.ProductId == row) {
                    result = true;
                }
            });
        });
        return result;
    },

    getCart: function () {
        return $.ajax({
            url: 'Cart/GetCart',
            method: "GET",
            success: function (data) { },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    showCart: function () {
    },

    onAddToCart: function () {
        alert("done")
    },

    loadCartTable: function () {
        return $('#cartTable').DataTable({
            processing: true,
            serverSide: true,
            "ajax": {
                url: "/Cart/LoadData",
                type: 'POST',
            },
            "columns": [
                { "data": "Id", "autoWidth": true,  },
                { "data": "ProductId", "autoWidth": true,  },
                { "data": "ProductName", "autoWidth": true },
                {
                    "data": null,
                    "render": function (data, type, row) {
                        return '<input type="number" min="1" Value=' + row.Quantity + ' class="form-control spinbox-input">';
                    },
                    "targets": 0,
                    "orderable": false


                },
                {
                    "data": null,
                    "className": "center",
                    "render": function (data, type, row) {
                        return '<input type="button" value="Delete" onclick="crudNamespace.deleteFromCart(' + row.Id + ')" class="btn btn-danger btn-xs">';
                    },
                    "targets": 0,
                    "orderable": false
                },
                { "data": "UserId", "autoWidth": true,  },
                { "data": "UserName", "autoWidth": true,  }
            ]
        });
    },


    deleteFromCart: function (id) {
        $.ajax({
            url: 'Cart/DeleteFromCart',
            method: "GET",
            data: { id: id },
            success: crudNamespace.onDeleteFromCart,
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    onDeleteFromCart: function () {
        cartTable.ajax.reload();
    },
};