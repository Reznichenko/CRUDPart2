﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OrderManagement.Startup))]
namespace OrderManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
