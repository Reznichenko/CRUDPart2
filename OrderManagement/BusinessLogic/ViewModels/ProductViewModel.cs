﻿namespace BusinessLogic.ViewModels
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public decimal Price { get; set; }
        public string Color { get; set; }
        public int VendorCode { get; set; }
        public int Quantity { get; set; }
    }
}
