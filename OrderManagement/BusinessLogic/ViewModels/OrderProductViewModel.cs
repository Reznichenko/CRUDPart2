﻿namespace BusinessLogic.ViewModels
{
    public class OrderProductViewModel
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public virtual OrderViewModel Order { get; set; }
        public virtual ProductViewModel Product { get; set; }
    }
}
