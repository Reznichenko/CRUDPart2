﻿using System;
using System.Collections.Generic;

namespace BusinessLogic.ViewModels
{
    public class AspNetUserViewModel
    {
        public AspNetUserViewModel()
        {
            this.Orders = new HashSet<OrderViewModel>();
        }

        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<OrderViewModel> Orders { get; set; }
    }
}
