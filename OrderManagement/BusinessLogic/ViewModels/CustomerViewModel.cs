﻿using System.Collections.Generic;

namespace BusinessLogic.ViewModels
{
    public class CustomerViewModel
    {
        public CustomerViewModel()
        {
            Orders = new HashSet<OrderViewModel>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<OrderViewModel> Orders { get; set; }
    }
}
