﻿using System.Collections.Generic;

namespace BusinessLogic.ViewModels
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            Products = new HashSet<ProductViewModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductViewModel> Products { get; set; }
    }
}
