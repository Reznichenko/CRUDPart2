﻿namespace BusinessLogic.Helper
{
    public class Direction
    {
        /// <summary>
        /// Gets "asc" value
        /// </summary>
        public static string Asc
        {
            get { return "asc"; }
        }
        /// <summary>
        /// Gets "desc" value
        /// </summary>
        public static string Desc
        {
            get { return "desc"; }
        }
    }
}
