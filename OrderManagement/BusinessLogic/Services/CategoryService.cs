﻿using BusinessLogic.Abstract;
using BusinessLogic.Entities;
using BusinessLogic.Extentions;
using BusinessLogic.Helper;
using BusinessLogic.Paging;
using BusinessLogic.ViewModels;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class CategoryService : IDisposable, IPagination<CategoryViewModel>
    {
        private readonly OrderManagementConnection _context = new OrderManagementConnection();

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<CategoryEntity> LoadEntity()
        {
            return _context.Categories.Select(x => new CategoryEntity()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }

        public List<CategoryViewModel> LoadViewModel(IEnumerable<CategoryEntity> entity)
        {
            return entity.Select(x => new CategoryViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }

        public IQueryable<CategoryViewModel> GetPaginated(DataTableAjaxPostModel model, out int totalRecords, out int recordsFiltered)
        {
            var data = _context.Categories.AsQueryable();
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(model.search.value))
            {
                data = Filter(model.search.value, data);
            }

            var query = (from category in data
                         select new CategoryViewModel
                         {
                             Id = category.Id,
                             Name = category.Name
                         }
                );

            recordsFiltered = data.Count();
            bool isAscending;
            var sortBy = Order(model, out isAscending);

            var result = query.Order(sortBy, isAscending).Skip((model.start * model.length)).Take(model.length);
            return result;
        }


        private static IQueryable<Category> Filter(string searchValue, IQueryable<Category> data)
        {
            return data.Where(x => x.Name.Contains(searchValue));
        }

        private static string Order(DataTableAjaxPostModel model, out bool isAscending)
        {
            var sortBy = "";
            isAscending = true;
            foreach (var column in model.order)
            {
                sortBy = model.columns[Convert.ToInt32(column.column)].data;
                isAscending = column.dir == Direction.Asc;
            }
            return sortBy;
        }

    }
}
