﻿using BusinessLogic.Entities;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class AspNetUserService : IDisposable
    {
        private readonly OrderManagementConnection _context = new OrderManagementConnection();

        public List<AspNetUserEntity> LoadEntity()
        {
            return _context.AspNetUsers.Select(x => new AspNetUserEntity()
            {
                Id = x.Id,
                Email = x.Email,
                EmailConfirmed = x.EmailConfirmed,
                PasswordHash = x.PasswordHash,
                SecurityStamp = x.SecurityStamp,
                PhoneNumber = x.PhoneNumber,
                PhoneNumberConfirmed = x.PhoneNumberConfirmed,
                TwoFactorEnabled = x.TwoFactorEnabled,
                LockoutEndDateUtc = x.LockoutEndDateUtc,
                LockoutEnabled = x.LockoutEnabled,
                AccessFailedCount = x.AccessFailedCount,
                UserName = x.UserName

            }).ToList();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
