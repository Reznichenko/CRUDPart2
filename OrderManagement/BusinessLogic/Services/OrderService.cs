﻿using BusinessLogic.Entities;
using BusinessLogic.Extentions;
using BusinessLogic.Helper;
using BusinessLogic.Paging;
using BusinessLogic.ViewModels;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class OrderService : IDisposable
    {
        private readonly OrderManagementConnection _context = new OrderManagementConnection();

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<OrderEntity> LoadEntity()
        {
            return _context.Orders.Select(x => new OrderEntity()
            {
                Id = x.Id,
                Date = x.Date,
                CustomerId = x.CustomerId,
                Status = x.Status,
                UserId = x.UserId,
            }).ToList();
        }


        public IQueryable<OrderViewModel> GetPaginated(DataTableAjaxPostModel model, out int totalRecords, out int recordsFiltered)
        {
            var data = _context.Orders.AsQueryable();
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(model.search.value))
            {
                data = Filter(model.search.value, data);
            }

            var query = (from order in data
                         join customer in _context.Customers on order.CustomerId equals customer.Id
                         join user in _context.AspNetUsers on order.UserId equals user.Id
                         join orderProduct in _context.OrderProducts on order.Id equals orderProduct.OrderId
                         join product in _context.Products on orderProduct.ProductId equals product.Id
                         select new OrderViewModel
                         {
                             Id = order.Id,
                             Date = order.Date,
                             CustomerId = order.CustomerId,
                             CustomerFirstName = customer.FirstName,
                             CustomerLastName = customer.LastName,
                             Status = order.Status,
                             UserId = order.UserId,
                             UserName = user.UserName,
                             ProductId = product.Id,
                             ProductName = product.Title
                         }
                );

            recordsFiltered = data.Count();
            bool isAscending;
            var sortBy = Order(model, out isAscending);

            var result = query.Order(sortBy, isAscending).Skip((model.start * model.length)).Take(model.length);
            return result;
        }


        private static IQueryable<DataAccessLayer.Models.Order> Filter(string searchValue, IQueryable<DataAccessLayer.Models.Order> data)
        {
            return data.Where(x => x.Status.Contains(searchValue)
                );
        }

        private static string Order(DataTableAjaxPostModel model, out bool isAscending)
        {
            var sortBy = "";
            isAscending = true;
            foreach (var column in model.order)
            {
                sortBy = model.columns[Convert.ToInt32(column.column)].data;
                isAscending = column.dir == Direction.Asc;
            }
            return sortBy;
        }


    }
}
