﻿using BusinessLogic.Entities;
using BusinessLogic.Extentions;
using BusinessLogic.Helper;
using BusinessLogic.Paging;
using BusinessLogic.ViewModels;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class CartService: IDisposable
    {
        private readonly OrderManagementConnection _context = new OrderManagementConnection();

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<CartEntity> LoadEntity()
        {
            return _context.Carts.Select(x => new CartEntity()
            {
                Id = x.Id,
                ProductId = x.ProductId,
                UserId = x.UserId,
                Quantity = x.Quantity
            }).ToList();
        }



        public List<CartViewModel> GetProductsInCart(string userId)
        {
            var data = _context.Carts.AsQueryable();
            var result = (from cart in data
                         where cart.UserId == userId
                         select new CartViewModel
                         {
                             Id = cart.Id,
                             ProductId = cart.ProductId
                         }
               );
            return result.ToList();
        }



        public List<CartViewModel> LoadViewModel(IEnumerable<ProductViewModel> productEntity, string userId)
        {
            var entity = LoadEntity();
            return entity.Select(x => new CartViewModel
            {
                Id = x.Id,
                ProductId = x.ProductId,
                ProductName = productEntity.First(d=>d.Id == x.ProductId).Title,
                UserId = _context.AspNetUsers.First(d=>d.Id == userId).Id,
                UserName = _context.AspNetUsers.First(d => d.Id == userId).UserName,
                Quantity = x.Quantity
            }).ToList();
        }


        public CartViewModel CreateViewModel(ProductViewModel productViewModel, string userId)
        {
            return new CartViewModel
            {
                ProductId = productViewModel.Id,
                ProductName = productViewModel.Title,
                UserId = _context.AspNetUsers.First(d => d.Id == userId).Id,
                UserName = _context.AspNetUsers.First(d => d.Id == userId).UserName,
                Quantity = 1
            };
        }


        public void AddToCart(CartViewModel cartViewModel)
        {
            _context.Carts.Add(
                new Cart
                {
                    Id = cartViewModel.Id,
                    ProductId = cartViewModel.ProductId,
                    UserId = cartViewModel.UserId,
                    Quantity = cartViewModel.Quantity

                });
            SaveChanges();
        }

        private void SaveChanges()
        {
            _context.SaveChanges();
        }


        public IQueryable<CartViewModel> GetPaginated(DataTableAjaxPostModel model, string userId, out int totalRecords, out int recordsFiltered)
        {
            var data = _context.Carts.AsQueryable();
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(model.search.value))
            {
                data = Filter(model.search.value, data);
            }

            var query = (from cart in data
                         join product in _context.Products on cart.ProductId equals product.Id
                         join aspnetUser in _context.AspNetUsers on cart.UserId equals aspnetUser.Id
                         where aspnetUser.Id == userId
                         select new CartViewModel
                         {
                             Id = cart.Id,
                             ProductId = product.Id,
                             ProductName = product.Title,
                             UserId = aspnetUser.Id,
                             UserName = aspnetUser.UserName,
                             Quantity = cart.Quantity
                         }
                );

            recordsFiltered = data.Count();
            bool isAscending;
            var sortBy = Order(model, out isAscending);

            var result = query.Order(sortBy, isAscending).Skip((model.start)).Take(model.length);
            return result;
        }


        private static IQueryable<Cart> Filter(string searchValue, IQueryable<Cart> data)
        {
            return data.Where(x => x.Product.Title.Contains(searchValue)
                || x.Quantity.ToString().Contains(searchValue));
        }

        private static string Order(DataTableAjaxPostModel model, out bool isAscending)
        {
            var sortBy = "";
            isAscending = true;
            foreach (var column in model.order)
            {
                sortBy = model.columns[Convert.ToInt32(column.column)].data;
                isAscending = column.dir == Direction.Asc;
            }
            return sortBy;
        }

        public void Delete(int id)
        {
            var cartDataModel = _context.Carts.Find(id);
            _context.Carts.Remove(cartDataModel);
            SaveChanges();
        }

    }
}
