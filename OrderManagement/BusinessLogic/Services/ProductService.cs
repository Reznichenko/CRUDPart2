﻿using BusinessLogic.Abstract;
using BusinessLogic.Entities;
using BusinessLogic.Extentions;
using BusinessLogic.Helper;
using BusinessLogic.Paging;
using BusinessLogic.ViewModels;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class ProductService : IDisposable, IPagination<ProductViewModel>
    {
        private readonly OrderManagementConnection _context = new OrderManagementConnection();

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<ProductEntity> LoadEntity()
        {
            return _context.Products.Select(x => new ProductEntity()
            {
                Id = x.Id,
                Title = x.Title,
                CategoryId = x.CategoryId,
                Price = x.Price,
                Color = x.Color,
                VendorCode = x.VendorCode,
                Quantity = x.Quantity
            }).ToList();
        }

        public IQueryable<ProductViewModel> GetPaginated(DataTableAjaxPostModel model, out int totalRecords, out int recordsFiltered)
        {
            var data = _context.Products.AsQueryable();
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(model.search.value))
            {
                data = Filter(model.search.value, data);
            }

            var query = (from product in data
                         join category in _context.Categories on product.CategoryId equals category.Id
                         select new ProductViewModel
                         {
                             Id = product.Id,
                             Title = product.Title,
                             CategoryName = category.Name,
                             CategoryId = product.CategoryId,
                             Color = product.Color,
                             VendorCode = product.VendorCode,
                             Price = product.Price,
                             Quantity = product.Quantity
                         }
                );

            recordsFiltered = data.Count();
            bool isAscending;
            var sortBy = Order(model, out isAscending);

            var result = query.Order(sortBy, isAscending).Skip((model.start)).Take(model.length);
            return result;
        }


        private static IQueryable<Product> Filter(string searchValue, IQueryable<Product> data)
        {
            return data.Where(x => x.Title.Contains(searchValue)
                || x.Color.Contains(searchValue)
                || x.Category.Name.Contains(searchValue)
                || x.Price.ToString().Contains(searchValue)
                || x.Quantity.ToString().Contains(searchValue)
                || x.VendorCode.ToString().Contains(searchValue));
        }

        private static string Order(DataTableAjaxPostModel model, out bool isAscending)
        {
            var sortBy = "";
            isAscending = true;
            foreach (var column in model.order)
            {
                sortBy = model.columns[Convert.ToInt32(column.column)].data;
                isAscending = column.dir == Direction.Asc;
            }
            return sortBy;
        }

        
        public List<ProductViewModel> LoadViewModel()
        {
            var entity = LoadEntity();
            return entity.Select(x => new ProductViewModel
            {
                Id = x.Id,
                Title = x.Title,
                CategoryId = x.CategoryId,
                Price = x.Price,
                Color = x.Color,
                VendorCode = x.VendorCode,
                Quantity = x.Quantity
            }).ToList();
        }

    }
}
