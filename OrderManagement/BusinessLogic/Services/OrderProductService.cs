﻿using BusinessLogic.Entities;
using BusinessLogic.ViewModels;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class OrderProductService : IDisposable
    {
        private readonly OrderManagementConnection _context = new OrderManagementConnection();

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<OrderProductEntity> LoadEntity()
        {
            return _context.OrderProducts.Select(x => new OrderProductEntity()
            {
                Id = x.Id,
                ProductId = x.ProductId,
                OrderId = x.OrderId,
                Quantity = x.Quantity
            }).ToList();
        }

        public List<OrderProductViewModel> LoadEntity(IEnumerable<OrderProductEntity> entity, IEnumerable<OrderViewModel> orderViewModel, IEnumerable<ProductViewModel> productViewModel)
        {
            return entity.Select(x => new OrderProductViewModel
            {
                Id = x.Id,
                Order = orderViewModel.First(y => y.Id == x.Id),
                Product = productViewModel.First(y => y.Id == x.Id),
                Quantity = x.Quantity

            }).ToList();
        }
    }
}