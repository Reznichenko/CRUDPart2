﻿using BusinessLogic.Entities;
using BusinessLogic.ViewModels;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class CustomerService : IDisposable
    {
        private readonly OrderManagementConnection _context = new OrderManagementConnection();

        public void Dispose()
        {
            _context.Dispose();
        }

        public List<CustomerEntity> LoadEntity()
        {
            return _context.Customers.Select(x => new CustomerEntity()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
        }

        public List<CustomerViewModel> LoadViewModel(IEnumerable<CustomerEntity> entity)
        {
            return entity.Select(x => new CustomerViewModel
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
        }
    }
}
