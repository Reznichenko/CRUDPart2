﻿using BusinessLogic.Paging;
using System.Linq;

namespace BusinessLogic.Abstract
{
    public interface IPagination<T> where T : class
    {
        IQueryable<T> GetPaginated(DataTableAjaxPostModel model, out int totalRecords, out int recordsFiltered);
    }
}
