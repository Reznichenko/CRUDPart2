﻿using System.Linq;
using System.Linq.Dynamic;

namespace BusinessLogic.Extentions
{
    public static class Extention
    {
        /// <summary>
        /// Sorts an elements of a sequence in ascending or descending based on switch.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="selector"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        public static IQueryable<T> Order<T>(this IQueryable<T> source, string selector, bool ascending)
        {
            return @ascending ? source.OrderBy(selector) : source.OrderBy(selector + " descending");
        }
    }
}
