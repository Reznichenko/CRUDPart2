﻿using System.Collections.Generic;

namespace BusinessLogic.Entities
{
    public class CategoryEntity
    {
        public CategoryEntity()
        {
            Products = new HashSet<ProductEntity>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductEntity> Products { get; set; }
    }
}
