﻿using System;
using System.Collections.Generic;

namespace BusinessLogic.Entities
{
    public class OrderEntity
    {
        public OrderEntity()
        {
            OrderProducts = new HashSet<OrderProductEntity>();
        }

        public int Id { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string Status { get; set; }
        public string UserId { get; set; }

        public virtual AspNetUserEntity AspNetUser { get; set; }
        public virtual CustomerEntity Customer { get; set; }

        public virtual ICollection<OrderProductEntity> OrderProducts { get; set; }
    }
}
