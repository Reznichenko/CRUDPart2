﻿namespace BusinessLogic.Entities
{
    public class CartEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string UserId { get; set; }

        public virtual AspNetUserEntity AspNetUser { get; set; }
        public virtual ProductEntity Product { get; set; }
    }
}
