﻿using System;
using System.Collections.Generic;

namespace BusinessLogic.Entities
{
    public class ProductEntity
    {
        public ProductEntity()
        {
            OrderProducts = new HashSet<OrderProductEntity>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public decimal Price { get; set; }
        public string Color { get; set; }
        public int VendorCode { get; set; }
        public int Quantity { get; set; }

        public virtual CategoryEntity Category { get; set; }
        public virtual ICollection<OrderProductEntity> OrderProducts { get; set; }
    }
}
